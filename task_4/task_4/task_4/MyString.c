#include "MyString.h"


int input_str(char ** str,FILE *file)
{
	char * temp = *str;
	int i = 0;


	*str = (char *)calloc(1,sizeof(char)); // выделяем память под один чар
	if( *str == NULL)
	{
		printf("\nCannot allocate string!\n");
		return 0;
	}

	do
	{

		(*str)[i] = fgetc(file); //да-да, тут будет каша из указателей и индексов =(

		if( (*str)[i] == '\n' )
		{
			(*str)[i] = '\0';
			break;
		}

		i++;
		temp = NULL;
		temp = (char *)realloc(*str,sizeof(char)*(i+1)); //добавим ещё один элемент в массив для записи конца строки. Да я знаю, что это медленно и нужно выделять блоками. Но пусть будет пока так.

		if(temp == NULL) // Используем временный указатель чтоб не было утечек. Потому как паямть освобождается только в том случае, если перевыделение было удачным, иначе указатель обнулится
		{
			printf("\nCannot reallocate string!\n");
			return 0;
		} else
		{
			*str = temp;
			temp = NULL;
			(*str)[i] = '\0';
		}



	} while (!NULL);


	return i;
};


void print_word(const WORD_PTR word, FILE * file)
{
	char * tmp = word.start;

	while(tmp < word.end)
	{
		putc(*tmp,file);
		tmp++;
	}

	return;
}

