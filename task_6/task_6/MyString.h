#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


typedef struct
{
	char * start;
	char * end;
} WORD_PTR;

int input_str(char ** str,FILE *file); //тут будем вводить строку откуда-нибудь. Возвращаем количество введённых символов.
void print_word(const WORD_PTR word, FILE * file);
int getint(char * message); // функция для ввода целых положительных чисел

