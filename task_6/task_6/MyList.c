#include "MyList.h"

void print_relative(const RELATIVE * current)
{
    printf("name: %s\nage: %d\n",current->name,current->age);
    return;
}

void print_relative_list(const RELATIVE * first)
{
    RELATIVE * current = first;

    while( current != NULL)
    {
        print_relative(current);
        current = current->next;
    }

    return;
}


void add_relative( RELATIVE * current, RELATIVE ** first, RELATIVE ** last) // вставляем, упорядочивая по возрасту
{
     RELATIVE * tmp_next = *first;
     RELATIVE * tmp_prev = *last;

     if( (*last) == NULL )
     {
         *first = current;
         *last = current;
         return;
     }

     if( tmp_next->age > current->age)
     {
         tmp_next->previos = current;
         current->next = tmp_next;
         (*first) = current;
         return;
     }

     if( tmp_prev->age < current->age)
     {
         tmp_prev->next = current;
         current->previos = tmp_prev;
         (*last) = current;
         return;
     }

     while( tmp_next->next != NULL )
     {

         if(tmp_next->age > current->age)
         {
             break;
         }
         tmp_next = tmp_next->next;

     }

     tmp_prev = tmp_next;
     tmp_next = tmp_prev->previos;

     tmp_next->next = current;
     tmp_prev->previos = current;
     current->next = tmp_prev;
     current->previos = tmp_next;

     return;
}


RELATIVE * input_relative(void)
{
    RELATIVE * current = NULL;
    char * tmp = NULL;
    int count = 0;

    printf("input name\n");

    count = input_str(&tmp,stdin);

    if(count < 1)
    {
        printf("invalid name!\n");
        return NULL;
    }

    current = (RELATIVE *)malloc(sizeof(RELATIVE));
    if(current == NULL)
    {
        printf("\ncan't allocate mem\n");
        return NULL;
    }

    current->name = (char*)malloc(sizeof(char)*(count+1));
    if(current->name == NULL)
    {
        printf("\ncan't allocate mem\n");
        return NULL;
    }
    strcpy(current->name,tmp);

    current->age = getint("input age");

    if(current->age < 1)
    {
        printf("invalid age!\n");
        free(current->name);
        free(current);
        return NULL;
    }

    current->next = NULL;
    current->previos = NULL;

    return current;
}

void delete_relative_list(RELATIVE ** first, RELATIVE ** last)
{
    RELATIVE * tmp = *first;

    *last = NULL;

    while( *first != NULL)
    {
        tmp = (*first)->next;
        free( (*first)->name);
        free( *first );
        *first = tmp;
    }
    return;
}
