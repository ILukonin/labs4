#pragma once

#include "MyString.h"

typedef struct
{
    struct RELATIVE * next;
    struct RELATIVE * previos;

    char * name;
    int age;

} RELATIVE; // ради интереса попробуем не массив, а двусвязный список.


void add_relative( RELATIVE * current, RELATIVE ** first, RELATIVE ** last); // добавление нового родственника
void print_relative_list(const RELATIVE * first); // печатаем список
RELATIVE* find_age(const RELATIVE * first,const int age); // ищем возраст
RELATIVE* find_name(const RELATIVE * first,const char * name); // ищем по имени

void print_relative(const RELATIVE * current); // печатаем один элемент
RELATIVE * input_relative(void); // ввод одного элемента
void delete_relative_list(RELATIVE ** first, RELATIVE ** last);//очистка списка
