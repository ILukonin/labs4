#include "MyList.h"

void print_help(void)
{
    printf("\nСпиоск родственников.\n");
    printf("Введите нужный пункт:\n");
    printf("1. Показать список.\n");
    printf("2. Добавить родственника.\n");
    printf("3. Удалить родственника\n");
    printf("4. Найти родственника\n");
    printf("5. Найти самого молодого и самого старого родственника\n");
    printf("6. Очистить список.\n");
    printf("7. Помощь\n");
    printf("8. Выход.\n\n");

}

int menu_select(void)
{

    return getint("введите номер нужного пункта");
}



int main(void)
{
    RELATIVE * first = NULL;
    RELATIVE * last = NULL;
    RELATIVE * current = NULL;


    print_help();

    for(;;)
    {
        switch (menu_select())
        {
        case 1:
            print_relative_list(first);
            break;
        case 2:
            current = input_relative();
            if(current == NULL)
                printf("Error!\n");
            else
                add_relative(current,&first,&last);
            break;
        case 3:
            printf("\n3\n");
            break;
        case 4:
            printf("\n4\n");
            break;
        case 5:
            printf("\nСамый молодой родственник: \n ");
            print_relative(first);
            printf("\nСамый старый родственник: \n ");
            print_relative(last);
            break;
        case 6:
            delete_relative_list(&first, &last);
            current = NULL;
            break;
        case 7:
            print_help();
            break;
        case 8:
            delete_relative_list(&first, &last);
            current = NULL;
            return 0;
            break;
        default:
            printf("\nНеверный ключ!\n");
            print_help();
            break;
        }
    }

    return 0;
}

